﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreManager : MonoBehaviour
{
    public GameObject storeUI;  // The entire UI
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        storeUI.SetActive(!storeUI.activeSelf);
    }

    private void OnTriggerExit(Collider other)
    {
        storeUI.SetActive(!storeUI.activeSelf);
    }

   
}
