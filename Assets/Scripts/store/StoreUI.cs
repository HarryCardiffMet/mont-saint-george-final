﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreUI : MonoBehaviour
{
	public Transform itemsParent;   // The parent object of all the items
	public GameObject storeUI;  // The entire UI
	public Item item;

	Store store;    // Our current inventory

	StoreSlot[] slots;  // List of all the slots

	void Start()
	{
		store = Store.instance;
		//store.onItemChangedCallback += UpdateUI;    // Subscribe to the onItemChanged callback

		// Populate our slots array
		slots = itemsParent.GetComponentsInChildren<StoreSlot>();
	}

	void UpdateUI()
	{
		// Loop through all the slots
		for (int i = 0; i < slots.Length; i++)
		{
			if (i < store.items.Count)  // If there is an item to add
			{
				slots[i].AddItem(store.items[i]);   // Add it
			}
			else
			{
				// Otherwise clear the slot
				slots[i].ClearSlot();
			}
		}
	}
	void PickUp()
	{
		Debug.Log("Picking up " + item.name);
		bool wasPickedUp = Inventory.instance.Add(item);

		if (wasPickedUp)
			Destroy(gameObject);
	}
}
