﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitScene : MonoBehaviour
{
    public void NextScene()
    {
        SceneManager.LoadScene("Blank");
    }
}
