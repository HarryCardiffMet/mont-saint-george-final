﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shopman : Interactable
{
    CurrencyManager coinM;
    //PlayerStats playerStats;
    
    public override void Interact()
    {
        base.Interact();

        AddCoin();

    }

    void Start()
    {
        coinM = GameObject.FindGameObjectWithTag("Manager").GetComponent<CurrencyManager>();
        //playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
    }
    
    void AddCoin()
    {
        coinM.Add(100);
        //playerStats.mushroomAdd();
        Destroy(gameObject);
        
    }
    

}
