﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : Interactable
{
    public override void Interact()
    {
        base.Interact();
        TriggerDiaglogue();
    }

    public Dialogue dialogue;

	void TriggerDiaglogue ()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }
}