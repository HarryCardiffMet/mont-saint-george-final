﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUp : Interactable
{
    //CurrencyManager coinM;
    PlayerStats playerStats;

    public override void Interact()
    {
        base.Interact();

        AddH();

    }

    void Start()
    {
        //coinM = GameObject.FindGameObjectWithTag("Manager").GetComponent<CurrencyManager>();
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
    }

    void AddH()
    {
        //coinM.Add(10);
        playerStats.mushroomAdd();
        Destroy(gameObject);

    }
}
