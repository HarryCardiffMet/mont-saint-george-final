﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyStats : CharacterStats
{

    CurrencyManager coinM;
    //bool coin = false;

    void Start()
    {
        coinM = GameObject.FindGameObjectWithTag("Manager").GetComponent<CurrencyManager>();
        
    }

    //private void Update()
    //{
   //     if (coin == true)
    //    {
    //        coinM.Add();
    //    }

   // }
    public override void Die()
    {
        base.Die();
        //Add loot
        //coin = true;
        coinM.Add(Random.Range(1,50));
        //TakeHealth(20);

        //Add Death Animation

        Destroy(gameObject);
        
    }

}
