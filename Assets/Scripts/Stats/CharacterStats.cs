﻿using UnityEngine;

/* Base class that player and enemies can derive from to include stats. */

public class CharacterStats : MonoBehaviour
{

    // Health
    public int maxHealth = 100;
    public int currentHealth { get; private set; }

    public HealthBar healthBar;

    public Stat damage;
    public Stat armor;
    public Stat inHealth;

    public event System.Action<int, int> OnHealthChanged;

    // Set current health to max health
    // when starting the game.
    void Awake()
    {
        currentHealth = maxHealth;
        
        if (healthBar != null)
        {
            healthBar.SetMaxHealth(maxHealth);
        }
    }

    void Update()
    {
        if (OnHealthChanged != null)
        {
            OnHealthChanged(maxHealth, currentHealth);
        }

        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    // Damage the character
    public void TakeDamage(int damage)
    {
        // Subtract the armor value
        damage -= armor.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue);

        

        // Damage the character
        currentHealth -= damage;
        Debug.Log(transform.name + " takes " + damage + " damage.");

        

        if (OnHealthChanged != null)
        {
            OnHealthChanged(maxHealth, currentHealth);
        }
        
        if (healthBar != null)
        {
            healthBar.SetHealth(currentHealth);
        }

        // If health reaches zero
        if (currentHealth <= 0)
        {
            Die();
        }
    }
    public void TakeHealth(int inHealth)
    {

        inHealth = Mathf.Clamp(inHealth, 0, 100);

        if (healthBar != null)
        {
            healthBar.SetHealth(currentHealth);
        }

        // Heal the character
        if (currentHealth < 100)
        {
            currentHealth += inHealth;
        }
        
        Debug.Log(transform.name + " takes " + inHealth + " Health.");

        if (OnHealthChanged != null)
        {
            OnHealthChanged(maxHealth, currentHealth);
        }

    }
    public virtual void Die()
    {
        // Die in some way
        // This method is meant to be overwritten
        Debug.Log(transform.name + " died.");
    }

}
