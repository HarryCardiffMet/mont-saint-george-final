﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.AI;

/* Handles the players stats and adds/removes modifiers when equipping items. */

public class PlayerStats : CharacterStats
{
	private Animator animator;

	// Use this for initialization
	void Start()
	{
		EquipmentManager.instance.onEquipmentChanged += OnEquipmentChanged;
	}

	void Update()
	{
		// Check to see if we should open/close the inventory
		if (Input.GetKeyDown(KeyCode.H))
		{
			//TakeHealth(1);
			
		}
		if (Input.GetKeyDown(KeyCode.G))
		{
			
			//TakeDamage(10);
		}
	}

	// Called when an item gets equipped/unequipped
	void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
	{
		// Add new modifiers
		if (newItem != null)
		{
			armor.AddModifier(newItem.armorModifier);
			damage.AddModifier(newItem.damageModifier);
		}

		// Remove old modifiers
		if (oldItem != null)
		{
			armor.RemoveModifier(oldItem.armorModifier);
			damage.RemoveModifier(oldItem.damageModifier);
		}

	}

	public void mushroomAdd()
    {
		TakeHealth(10);
	}

	public override void Die()
	{
		base.Die();
		PlayerManager.instance.KillPlayer();
	}
}
