﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossDeath : MonoBehaviour
{

    // Start is called before the first frame update
    #region Singleton

    public static BossDeath instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    public GameObject Boss;

    public void KillBoss ()
    {
        SceneManager.LoadScene("GameOver");
    }
}
