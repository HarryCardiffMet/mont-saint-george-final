﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class BossStats : CharacterStats
{

    CurrencyManager coinM;
    //bool coin = false;

    void Start()
    {
        coinM = GameObject.FindGameObjectWithTag("Manager").GetComponent<CurrencyManager>();

    }


    public override void Die()
    {
        base.Die();

        coinM.Add(1000);

        Destroy(gameObject);
        SceneManager.LoadScene("GameOver");

    }

}
