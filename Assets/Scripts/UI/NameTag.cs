﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameTag : MonoBehaviour
{

    public GameObject uiPrefab;
    public Transform target;

    Transform ui;
    Text nametag;

    Transform cam;


    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.transform;

        foreach (Canvas c in FindObjectsOfType<Canvas>())
        {
            if (c.renderMode == RenderMode.WorldSpace)
            {
                ui = Instantiate(uiPrefab, c.transform).transform;
                nametag = ui.GetChild(0).GetComponent<Text>();
                break;
            }
        }
        

    }
    
    void LateUpdate()
    {
        ui.position = target.position;
        ui.forward = -cam.forward;
       
    }
}
