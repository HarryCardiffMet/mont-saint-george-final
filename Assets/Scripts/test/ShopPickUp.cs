﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopPickUp : Interactable
{
    public Item item;

    public override void Interact()
    {
        base.Interact();

        PickUp();
    }

    void PickUp()
    {
        Debug.Log("Picking up " + item.name);
        bool wasPickedUp = Store.instance.Add(item);

        if (wasPickedUp)
            Destroy(gameObject);
    }
}
