﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrencyManager : MonoBehaviour
{
    public Text text;
    public int currency;
    public int newCurrency;
    
    public event System.Action<int> OnCoinsChanged;
    //public Coins coins;



    void Update ()
    {
        if (OnCoinsChanged != null)
        {
            OnCoinsChanged(currency);
        }
            

        text.text = currency.ToString("0");
        //if (coins.coinsAdd)
        //{
        //newCurrency = currency + 100;
        //currency = newCurrency;
        //text.text = "" + currency;
        //coinsAdd = false;

        //}

    }

    public void Add(int newCurrency)
    {
        currency += newCurrency;
    }
  

}
