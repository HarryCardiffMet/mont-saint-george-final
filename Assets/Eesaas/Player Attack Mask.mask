%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Player Attack Mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: geo
    m_Weight: 1
  - m_Path: geo/Armour
    m_Weight: 1
  - m_Path: geo/Cloth
    m_Weight: 1
  - m_Path: geo/Dagger
    m_Weight: 1
  - m_Path: geo/Hair
    m_Weight: 1
  - m_Path: geo/Head
    m_Weight: 1
  - m_Path: geo/Scarf
    m_Weight: 1
  - m_Path: geo/Sword
    m_Weight: 1
  - m_Path: SK_RFA
    m_Weight: 0
  - m_Path: SK_RFA/root
    m_Weight: 0
  - m_Path: SK_RFA/root/ik_foot_root
    m_Weight: 0
  - m_Path: SK_RFA/root/ik_foot_root/ik_foot_l
    m_Weight: 0
  - m_Path: SK_RFA/root/ik_foot_root/ik_foot_r
    m_Weight: 0
  - m_Path: SK_RFA/root/ik_hand_root
    m_Weight: 0
  - m_Path: SK_RFA/root/ik_hand_root/ik_hand_gun
    m_Weight: 0
  - m_Path: SK_RFA/root/ik_hand_root/ik_hand_gun/ik_hand_l
    m_Weight: 0
  - m_Path: SK_RFA/root/ik_hand_root/ik_hand_gun/ik_hand_r
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_1_1
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_1_1/cloth_1_2
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_1_1/cloth_1_2/cloth_1_3
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_2_1
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_2_1/cloth_2_2
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_2_1/cloth_2_2/cloth_2_3
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_2_1/cloth_2_2/cloth_2_3/cloth_2_4
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_2_1/cloth_2_2/cloth_2_3/cloth_2_4/cloth_2_5
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_2_1/cloth_2_2/cloth_2_3/cloth_2_4/cloth_2_5/cloth_2_6
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_3_1
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_3_1/cloth_3_2
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_3_1/cloth_3_2/cloth_3_3
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_3_1/cloth_3_2/cloth_3_3/cloth_3_4
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_3_1/cloth_3_2/cloth_3_3/cloth_3_4/cloth_3_5
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_4_1
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_4_1/cloth_4_2
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_4_1/cloth_4_2/cloth_4_3
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_4_1/cloth_4_2/cloth_4_3/cloth_4_4
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_5_1
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_5_1/cloth_5_2
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_5_1/cloth_5_2/cloth_5_3
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_5_1/cloth_5_2/cloth_5_3/cloth_5_4
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_6_1
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_6_1/cloth_6_2
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_6_1/cloth_6_2/cloth_6_3
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_6_1/cloth_6_2/cloth_6_3/cloth_6_4
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/cloth_6_1/cloth_6_2/cloth_6_3/cloth_6_4/cloth_6_5
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/extra_1
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/extra_1/extra_1End
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/spine_01
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/boobs_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/boobs_l/boobs_end_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/boobs_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/boobs_r/boobs_end_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/dagger
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/dagger/dagger_stretch
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/dagger/dagger_stretch/dagger_stretch_1
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/dagger/dagger_stretch/dagger_stretch_1/dagger_stretch_2_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/dagger/dagger_stretch/dagger_stretch_1/dagger_stretch_2_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/dagger/dagger_stretch/dagger_stretch_1_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/dagger/dagger_stretch/dagger_stretch_1_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/index_01_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/index_01_l/index_02_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/index_01_l/index_02_l/index_03_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/middle_01_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/middle_01_l/middle_02_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/middle_01_l/middle_02_l/middle_03_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/pinky_01_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/pinky_01_l/pinky_02_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/pinky_01_l/pinky_02_l/pinky_03_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/ring_01_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/ring_01_l/ring_02_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/ring_01_l/ring_02_l/ring_03_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/thumb_01_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/thumb_01_l/thumb_02_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/thumb_01_l/thumb_02_l/thumb_03_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/lowerarm_twist_01_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/upperarm_twist_01_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/index_01_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/index_01_r/index_02_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/index_01_r/index_02_r/index_03_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/middle_01_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/middle_01_r/middle_02_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/middle_01_r/middle_02_r/middle_03_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/pinky_01_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/pinky_01_r/pinky_02_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/pinky_01_r/pinky_02_r/pinky_03_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/ring_01_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/ring_01_r/ring_02_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/ring_01_r/ring_02_r/ring_03_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/sword
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/sword/sword_stretch
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/sword/sword_stretch/sword_stretch_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/sword/sword_stretch/sword_stretch_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/thumb_01_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/thumb_01_r/thumb_02_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/thumb_01_r/thumb_02_r/thumb_03_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/lowerarm_twist_01_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/upperarm_twist_01_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/bottom_lip_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/bottom_lip_l/bottom_lip_end_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/bottom_lip_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/bottom_lip_r/bottom_lip_end_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/eye_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/eye_l/eye_end_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/eye_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/eye_r/eye_end_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/mouth
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/mouth/mouth_end
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/upper_lip_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/upper_lip_l/upper_lip_end_l
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/upper_lip_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/spine_01/spine_02/spine_03/neck_01/head/upper_lip_r/upper_lip_end_r
    m_Weight: 1
  - m_Path: SK_RFA/root/pelvis/thigh_l
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/thigh_l/calf_l
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/thigh_l/calf_l/calf_twist_01_l
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/thigh_l/calf_l/foot_l
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/thigh_l/calf_l/foot_l/ball_l
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/thigh_l/thigh_twist_01_l
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/thigh_r
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/thigh_r/calf_r
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/thigh_r/calf_r/calf_twist_01_r
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/thigh_r/calf_r/foot_r
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/thigh_r/calf_r/foot_r/ball_r
    m_Weight: 0
  - m_Path: SK_RFA/root/pelvis/thigh_r/thigh_twist_01_r
    m_Weight: 0
