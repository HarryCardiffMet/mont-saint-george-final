﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    public List<ItemK> items = new List<ItemK>();

    private void Awake()
    {
        BuildDatabase();
    }

    public ItemK GetItem(int id)
    {
        return items.Find(item => item.id == id);
    }

    public ItemK GetItem(string itemName)
    {
        return items.Find(item => item.title == itemName);
    }
    void BuildDatabase()
    {
        items = new List<ItemK>()
        {
            new ItemK(0, "Diamond sword", "A sword made with diamond.",
            new Dictionary<string, int>
            {
                {"Power", 15 },
                {"Defence", 10 }
            })
        };
    }
           
  
}
